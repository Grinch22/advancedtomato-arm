# **Grinch's AdvancedTomato-ARM** #

**UPDATE: I'm using FreshTomato now, so there will no longer be any updates to this repo for the foreseeable future**

**Forked from AndreDVJ's AdvancedTomato-ARM, builds compiled by Grinch**

This is AdvancedTomato with the latest fixes and enhancements from FreshTomato. I have added some of my own modifications, which include a compact GUI, display of hostnames in the IP traffic graphs, restoration of Stealth mode, bug fixes and cosmetic tweaks.

This is the firmware I use in my RT-AC68U routers; I have made downloads available in the Downloads link below. I have also included builds for some other popular routers; just keep in mind that they are not tested.

If you would like to pick up any of my changes and merge them to your repository, feel free to do so. That's the reason Tomato is an open-source project.

**Source code**: https://bitbucket.org/Grinch22/advancedtomato-arm/commits/all

**Downloads**: https://mega.nz/#F!7WhjCRzK!H5j_JG-1a4ucpDBPBkwXQw
